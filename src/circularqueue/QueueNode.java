package circularqueue;
public class QueueNode {
    protected QueueNode next;
    protected String data;
    QueueNode(String data){
        this.data=data;
        next=null;
    }

    @Override
    public String toString() {
        return "Data = " + data;
    }
    public void displayQueueNode(){
            System.out.print(data+ " ");
        }
    public QueueNode getNext() {
        return next;
    }

    public void setNext(QueueNode next) {
        this.next = next;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
