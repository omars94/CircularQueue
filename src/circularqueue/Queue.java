package circularqueue;

public class Queue {

    QueueNode front;
    QueueNode rear;

    public Queue() {
    }

    public boolean isEmpty() {
        return (front == null);
    }

    public void addLast(String data) {
        QueueNode n = new QueueNode(data);
        if (isEmpty()) {
            n.next = front;
            front = n;
            rear = n;
            rear.next = front;
        } else {
            rear.next = n;
            rear = n;
            rear.next = front;
        }
    }

    public QueueNode removeFirst() {
        QueueNode temp = front;
        if (front.next == null) {
            rear = null;
        }
        front = front.next;
        rear.next = front;
        return temp;
    }

    public void displayList() {
        QueueNode current = front;
        while (current != null) {
            if (current == rear) {
                System.out.println(current.data);
                break;
            } else {
                System.out.println(current.data);
                current = current.next;
            }
        }
    }
}
